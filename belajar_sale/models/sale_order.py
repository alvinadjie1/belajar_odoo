# -*- coding: utf-8 -*-
"""file purchase order"""
from odoo import models, fields, api, _
# from odoo.exceptions import UserError, Warning, ValidationError

class SaleOrder(models.Model):
    """inherit models sale.order"""
    _inherit = 'sale.order'
    
    # field
    keterangan = fields.Char()
    # many2one
    product_id = fields.Many2one('product.product')
    product_price = fields.Float(related="product_id.lst_price")
    invoice_id = fields.Many2one('account.move')
    dokumen = fields.Binary()

    

    

    
